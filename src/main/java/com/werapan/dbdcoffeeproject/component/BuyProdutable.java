/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.werapan.dbdcoffeeproject.component;

import com.werapan.dbdcoffeeproject.model.Product;

/**
 *
 * @author Paweena Chinasri
 */
public interface BuyProdutable {
    public void buy(Product product, int qty);
}
